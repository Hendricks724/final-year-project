﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour
{

    [Header("Game Objects:")]
    public GameObject sectionHolder;
    public GameObject vehicleHolder;
    public GameObject player;
    public GameObject spawnBuffer;

    [Header("Ground Params")]
    public int activeRoadTiles = 10;
    public GameObject tilePrefab;

    [Header("Vehicles:")]
    public GameObject truck;

    //private vars
    private GameObject section = null;
    private GameObject vehicle = null;

    private List<GameObject> sections;
    private List<float> spawnPositions;
    private List<GameObject> vehicles;
    private float currentSpawnPosition;
    private float spawnRange = 500f;
    private bool spawn;
    

    void Start()
    {

        //Hold instantiated sections and vehicles
        sections = new List<GameObject>();
        spawnPositions = new List<float>();
        vehicles = new List<GameObject>();


        //setting initial positions
        player.transform.position += Vector3.forward * 16.0f * tilePrefab.transform.localScale.z* activeRoadTiles / 2.0f;
        spawnBuffer.transform.position += Vector3.forward * 16.0f * tilePrefab.transform.localScale.z* activeRoadTiles / 2.0f;

        currentSpawnPosition = 0;
        spawn = true;

        initialBuild();
    }

    public void initialBuild()
    {

        //Add on the extra straight end instead of the turn.
        //if (straightBool)
        //{
        //    segmentBufferSize++;
        //}

        for (int x = 0; x < activeRoadTiles ; x++)
        {
            section = (GameObject)Instantiate(tilePrefab, transform.position, transform.rotation);
            section.transform.parent = sectionHolder.transform;
            sections.Add(section);
            if (x > activeRoadTiles / 2 -2)
            {
                spawnPositions.Add(section.transform.position.z);
                spawnPositions.Add(section.transform.position.z+8f);//assuming z size of tile is 16
                //spawnVehicle();
            }
            transform.position +=transform.forward * 16.0f * tilePrefab.transform.localScale.z;
        }

        
        //Turn off vehicles till we start moving.
        //vehicleHolder.SetActive(false);
    }

    public void spawnTile()
    {
        //spawnCounter+=2;

        //Removing the first tile in the buffer and destroying the gameobject- FIFO system.
        Destroy(sections[0]);
        sections.RemoveAt(0);
        spawnPositions.RemoveAt(0);
        spawnPositions.RemoveAt(0);

        //spawnVehicle();
        //Handling special straights after turns

        section = (GameObject)Instantiate(tilePrefab, transform.position, transform.rotation);

        section.transform.parent = sectionHolder.transform;

        sections.Add(section);
        spawnPositions.Add(section.transform.position.z);
        spawnPositions.Add(section.transform.position.z+8f);
        transform.position = transform.position + (transform.forward * 16.0f * tilePrefab.transform.localScale.z);
    }

    public void spawnObjectsRandom(GameObject obj, int spawnDistance, float probability,bool fullLane)
    {
        spawn = true;
        //setting start spawn position to player z
        currentSpawnPosition = player.transform.position.z;
        //while there is still area to spawn
        while(currentSpawnPosition < this.spawnRange)
        {
            //current spawn position incrementing by spawn distance
            currentSpawnPosition += spawnDistance;

            int laneRand = Random.Range(1, 4);

            //in case we want randomness, we could omit some spawns based on given probability
            if (Random.value <= probability)
            {
                //if the object occupies the entire lane
                if (fullLane)
                {
                    Instantiate(obj, new Vector3(0, 20, currentSpawnPosition), Quaternion.identity, vehicleHolder.transform);
                }
                else
                {
                    Instantiate(obj, new Vector3((laneRand - 2) * 2, 20, currentSpawnPosition), Quaternion.identity, vehicleHolder.transform);
                }

            }
            
        }

        //to continue spawing as the player proceeds
        StartCoroutine(spawnContinuation(obj, spawnDistance, probability, fullLane));
    }

    public void stopSpawning()
    {
        spawn = false;
    }

    private IEnumerator spawnContinuation(GameObject obj, int spawnDistance,float probability,bool fullLane)
    {
        while (spawn)
        {
            //unspawnedLength is the road length for which objects have not been spawned yet. If it is larger than spawn distance we must spawn at the next spawn point.
            float unspawnedLength = player.transform.position.z + spawnRange - currentSpawnPosition;
            while (unspawnedLength >= spawnDistance)//if its time to spawn a new object
            {

                currentSpawnPosition += spawnDistance;
                //unspawned length decremented since we are about to spawn
                unspawnedLength -= spawnDistance;

                //identical to spawn
                if (Random.value <= probability)
                {
                    if (fullLane)
                    {
                        Instantiate(obj, new Vector3(0, 20, currentSpawnPosition), Quaternion.identity, vehicleHolder.transform);
                    }
                    else
                    {
                        Instantiate(obj, new Vector3((Random.Range(1, 4) - 2) * 2, 20, currentSpawnPosition), Quaternion.identity, vehicleHolder.transform);
                    }

                }
            }
            yield return new WaitForSeconds(1);
        }
       
    }


    void spawnVehicle()
    {

        //set the spawn buffer new position
        spawnBuffer.transform.position = transform.position;
        spawnBuffer.transform.rotation = transform.rotation;

        int lane;

            
        lane = Random.Range(1, 4);

            
        vehicle = (GameObject)Instantiate(truck, spawnBuffer.transform.position + (spawnBuffer.transform.right * (lane - 2) * 2), spawnBuffer.transform.rotation);
        vehicle.transform.parent = vehicleHolder.transform;
        vehicles.Add(vehicle);
        VehicleBehaviour tempReference = vehicles[vehicles.Count - 1].GetComponent<VehicleBehaviour>();
        
       
    }

    public void removeVehicle()
    {
        Destroy(vehicles[0]);
        vehicles.RemoveAt(0);
    }

    public void restartSprint()
    {
        //Destroy everything then initial build.
        while (vehicles[0] == null)
        {
            vehicles.RemoveAt(0);
        }
        while(vehicles.Count != 0)
        {
            removeVehicle();
        }
        while(sections.Count != 0)
        {
            Destroy(sections[0]);
            sections.RemoveAt(0);
        }

        transform.position = Vector3.zero;
        initialBuild();
    }

    //public void endSession(bool beatGhost)
    //{
    //    float[] playerSpeedRecording;
    //    float[] playerLeanRecording;

    //    //Convert track recording to array and save if it was LONGER.
    //    if (sectionRecording.Count > prevSectionRecording.Length)
    //    {
    //        prevSectionRecording = sectionRecording.ToArray();
    //    }
    //    if(truckRecording.Count > prevTruckRecording.Length)
    //    {
    //        prevTruckRecording = truckRecording.ToArray();
    //    }
    //    Time.timeScale = 0;

    //    //Get ghost information only if score is higher otherwise its the same
    //    if (beatGhost)
    //    {
    //        playerSpeedRecording = player.GetComponent<PlayerController>().getPlayerSpeedRecording();
    //        playerLeanRecording = player.GetComponent<PlayerController>().getPlayerLeanRecording();
    //    }
    //    else
    //    {
    //        playerSpeedRecording = DataSaver.saver.ghostSpeedRecording;
    //        playerLeanRecording = DataSaver.saver.ghostLeanRecording;
    //    }
        
    //    DataSaver.saver.Save(prevSectionRecording, prevTruckRecording, playerSpeedRecording, playerLeanRecording);
    //    resultsButton.SetActive(true);
    //}
}