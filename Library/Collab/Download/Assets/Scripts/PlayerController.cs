﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float hitForce;
    public float hitRange;
    [SerializeField]
    GameManager control;

    [SerializeField]
    SerialPortCommunicator SPC;

    [SerializeField]
    GameObject headset, tilt;

    private List<float> playerSpeedRecording = new List<float>();
    private List<float> playerLeanRecording = new List<float>();

    private Rigidbody playerRigidbody;
    
    public float forwardSpeed = 10.0f, horizontalSpeed = 5.0f, maxLean = 35.0f, rpmDividor = 100.0f;

    private float forwardProduct;

    private Vector3 forwardVelocity, horizontalVelocity, rotatePoint;

    private bool rotateLeft, rotateRight;

    private float degreesLeft;
    private float degreesRight;
    private float totalRotation = 0.0f;
    private float rotationStep;

    private bool noLeft = false, noRight = false, noBuffer = false;

    public bool freezeMovement = false;
    public PlayerBodyController playerBodyController;
    public Animator hitAnimator;

    [SerializeField]
    PoliceBehaviour police;

    void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody>();
}
    // Update is called once per frame
    void FixedUpdate()
    {
        //This is where we will put a function to sort out 
        //float v = calculateSpeed();
        //float h = calculateLean();
        //Move(v,h);
    }

    float calculateSpeed() //GIve a value between 0 and one based on bike.
    {
        float speed;
        if (freezeMovement)
        {
            speed = 0.0f;
        }
        else
        {
            //speed = SPC.rpm / rpmDividor;
            speed = Input.GetAxisRaw("Vertical");
            //speed = playerBodyController.currentMovement;
        }
        
        playerSpeedRecording.Add(speed);
        return speed;
        
    }

    float calculateLean()
    {
        float lean;
        if (freezeMovement)
        {
            lean = 0.0f;
        }
        else
        {
            //Vive headset
            
            float rotation = headset.transform.eulerAngles.z;
            if(rotation > 90)
            {
                rotation -= 360;
            }

            lean = -rotation / maxLean;
      
            //lean = Input.GetAxisRaw("Horizontal");
            if (noLeft && lean < 0.0f)
            {
                lean = 0.0f;
            }
            else if (noRight && lean > 0.0f)
            {
                lean = 0.0f;
            }
        }

        playerLeanRecording.Add(lean);
        return lean;
    }

    void Move(float v, float h)
    { //Handles movement forwards and backwards
        
        if (rotateLeft)
        {
            TurnLeft(v);
        }
        else if (rotateRight)
        {
            TurnRight(v);
        }
        else
        {
            forwardProduct = forwardSpeed * v;
            forwardVelocity = playerRigidbody.transform.forward * forwardProduct;
            //police.setPoliceSpeed(forwardProduct);
        }

        horizontalVelocity = playerRigidbody.transform.right * horizontalSpeed * h;
        //Setting the bike rotation
        Vector3 angle = tilt.transform.eulerAngles;
        angle.z = h * maxLean / 3.0f;
        tilt.transform.eulerAngles = angle;

        playerRigidbody.velocity = forwardVelocity + horizontalVelocity;
    }

    void TurnLeft(float v)
    {
        //Float difference between position and rotation point
        float seperation = Vector3.Distance(transform.position, rotatePoint);
        forwardProduct = v * forwardSpeed;
        police.setPoliceSpeed(forwardProduct * (8.0f / seperation));

        degreesLeft = (-90 * forwardProduct) / (0.5f * Mathf.PI * seperation);

        rotationStep = degreesLeft * Time.fixedDeltaTime;
        if (rotationStep < -90.0f - totalRotation)
        {
            rotationStep = -90.0f - totalRotation;
            totalRotation = 0.0f;
            rotateLeft = false;
        }
        else
        {
            totalRotation += rotationStep;
        }
        transform.RotateAround(rotatePoint, Vector3.up, rotationStep);
    }

    void TurnRight(float v)
    {
        float seperation = Vector3.Distance(transform.position, rotatePoint);
        forwardProduct = v * forwardSpeed;
        police.setPoliceSpeed(forwardProduct * (8.0f / seperation));

        degreesRight = (90 * forwardProduct) / (0.5f * Mathf.PI * seperation);

        rotationStep = degreesRight * Time.fixedDeltaTime;
        if (rotationStep > 90.0f - totalRotation)
        {
            rotationStep = 90.0f - totalRotation;
            totalRotation = 0.0f;
            rotateRight = false;
        }
        else
        {
            totalRotation += rotationStep;
        }
        transform.RotateAround(rotatePoint, Vector3.up, rotationStep);
    }

    public float[] getPlayerSpeedRecording()
    {
        return playerSpeedRecording.ToArray();
    }

    public float[] getPlayerLeanRecording()
    {
        return playerLeanRecording.ToArray();
    }

    //called by the player body controller, to notify of a trigger collision
    public void onBodyTrigger(Collider other)
    {
        if (other.tag == "LeftTurn")
        {
            forwardVelocity = Vector3.zero;
            rotatePoint = other.transform.Find("RotationPoint").transform.position;
            rotateLeft = true;
            control.spawnTile();
        }
        else if (other.tag == "RightTurn")
        {
            forwardVelocity = Vector3.zero;
            rotatePoint = other.transform.Find("RotationPoint").transform.position;
            rotateRight = true;
            control.spawnTile();
        }
        else if(other.tag == "Straight")
        {
            control.spawnTile();
        }
        else if (other.tag == "Vehicle")
        {
            //Destroy truck we will ahve to then in the GM remove the null vehicle gameobjects.
            //Destroy(other.transform.parent.gameObject);
            //Increase resitance or slow user / minus points.
            //SPC.crashResistance = 1.15f;
            other.transform.parent.GetComponent<VehicleBehaviour>().enabled = false;
            Rigidbody truckBody = other.transform.parent.GetComponent<Rigidbody>();
            truckBody.freezeRotation = false;
            motionEnhancer motionEnhancer = GetComponent<motionEnhancer>();
            //truckBody.AddExplosionForce(hitForce, transform.position , hitRange);
            truckBody.AddRelativeForce(Random.Range(-100*Mathf.Pow(motionEnhancer.avatarSpeed, 1.5f), 100*Mathf.Pow(motionEnhancer.avatarSpeed, 1.5f)), Random.Range(0, 500*Mathf.Pow(motionEnhancer.avatarSpeed, 1.5f)), Random.Range(Mathf.Pow(100*motionEnhancer.avatarSpeed, 1.5f), Mathf.Pow(200*motionEnhancer.avatarSpeed, 1.5f)));

            truckBody.AddTorque(Random.Range(-motionEnhancer.avatarSpeed * 5000, motionEnhancer.avatarSpeed * 5000), Random.Range(-motionEnhancer.avatarSpeed * 5000, motionEnhancer.avatarSpeed * 5000), Random.Range(-motionEnhancer.avatarSpeed * 5000, motionEnhancer.avatarSpeed * 5000));
        }
        else if (other.tag == "LeftBox")
        {
            if(noLeft == true)
            {
                noBuffer = true;
            }
            else
            {
                noLeft = true;
            }
        }
        else if (other.tag == "RightBox")
        {
            if (noRight == true)
            {
                noBuffer = true;
            }
            else
            {
                noRight = true;
            }
        }
        else if (other.tag == "Lava")
        {
            hitAnimator.SetTrigger("hit");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "LeftBox")
        {
            if (noBuffer)
            {
                noBuffer = false;
            }
            else
            {
                noLeft = false;
            }
        }
        else if (other.tag == "RightBox")
        {
            if (noBuffer)
            {
                noBuffer = false;
            }
            else
            {
                noRight = false;
            }
        }
    }
}
