﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class astronautController : MonoBehaviour {

    public float speed;
    public float finalSpeed;
    public float distanceWeight;
    public float increaseRate;
    public float attackPenalty;
    public float attackRange;

    public Transform player;
    public motionEnhancer motionEnhancer;
    public bool active;
    private float previousDistance;

    private void Start()
    {
        speed = 0;
        //StartCoroutine(CalculateSpeed());
    }

    void Update()//NOTE: CHANGE THIS FROM UPDATE TO FIXEDUPDATE
    {
        if (transform.position.z < player.position.z - attackRange || finalSpeed<0) //astronaut cant get ahead of the player
        {
            transform.position += Vector3.forward * finalSpeed * Time.deltaTime;
        }
        
        GetComponent<Animator>().SetFloat("runMultiplier", finalSpeed / 5);
    }


    public float minMagnitude;
    public float maxMagnitude;
    public float roughness;
    public float fadeInTime;
    public float fadeOutTime;

    public void stomp()
    {
        CameraShaker.Instance.ShakeOnce(Mathf.Lerp(minMagnitude, maxMagnitude, Mathf.InverseLerp(50,0, player.transform.position.z - this.transform.position.z)), roughness, fadeInTime, fadeOutTime);
    }

    IEnumerator UpdateSpeed()
    {
        //set initial speed
        speed = motionEnhancer.calculateBaseRunningSpeed(0.1f);//astronaut starts at 0.1 player max speed
        while (active)
        {
            float distance = player.transform.position.z - this.transform.position.z;
            if (distance > previousDistance)//player is gaining distance on the astronaut
            { 
                //speed +=Mathf.Clamp(distance-attackRange,0,1)* motionEnhancer.calculateBaseRunningSpeed(increaseRate); 
                
                speed += Mathf.InverseLerp(attackRange + 1, attackRange + 7, distance) * motionEnhancer.calculateBaseRunningSpeed(increaseRate);//added a distance factor to moderate speed increase once the astronaut gets too close, so that the astronaut is not bound to catch the player. Also, gain is measured in terms of max player speed so that it automatically adjusts with different speeds
                //so if distance from player is more than attackRange+7, astronaut gains on the player normally, if closer, gain lessens until we reach distance attack range+1 where there is no gain. At that point the player simply needs to maintain his speed
            }

            if (distance <= attackRange)
            {
                //astronaut attacks player
                speed -= motionEnhancer.calculateBaseRunningSpeed(attackPenalty);
            }

            finalSpeed = speed + distanceWeight * distance;
            yield return new WaitForSeconds(0.5f);
        }
       
    }

    //IEnumerator UpdateSpeedFar()
    //{
    //    float distance = player.transform.position.z - this.transform.position.z;
    //    while (distance > catchUpThreshold)
    //    {
    //        speed = distance;
    //        distance = player.transform.position.z - this.transform.position.z;
    //        yield return new WaitForSeconds(0.01f);
    //    }
    //    //set initial speed
    //    speed = motionEnhancer.calculateBaseRunningSpeed(0.7f);//astronaut starts at 0.7 player max speed
    //    StartCoroutine(UpdateSpeedClose());
    //}

    public void activate()
    {
        active = true;
        StartCoroutine(UpdateSpeed());
    }

    public void deactivate()
    {
        active = false;
        finalSpeed = 0;
    }
}
