﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Enum for current exergame personality condition
[System.Serializable]
public enum personalityType{ 
	Conqueror,
	Survivor
}

//Enum for grouped game components for personality conditions
[System.Serializable]
public struct gameComponent{
	public GameObject gObject;
	public float probability;
}


//Singleton Game Manager//
public class GameManager : MonoBehaviour
{

    [Header("Game Objects:")]
    public GameObject sectionHolder; //Holds all tile sections
	public GameObject chaser;
	public GameScreenManager gui;


    [Header("Ground Params")]
    public int activeRoadTiles = 10;
	public GameObject conquerorPrefab;
	public GameObject survivorPrefab;

    [Header("Game Elements:")]
    //public GameObject truck;
	public List<gameComponent> conquerorGameComponents = new List<gameComponent> (); //list of game components for conqueror & probabilities
	public List<gameComponent> survivorGameComponents = new List<gameComponent> (); //list of game components for conqueror & probabilities
	public int maxCount = 5; //maximum count of game components per tile
	public List<GameObject> enemyList = new List<GameObject>(); //list of all enemies

	[Header("Personality Type")]
	public personalityType gameCondition;

    //private vars
	private static GameManager _instance;
	private GameObject section = null;
    private bool spawn;

    private List<GameObject> sections = new List<GameObject>(); //Holds all the currently created tiles
	private List<levelTile> activeSections = new List<levelTile>(); //Holds list of all sections active and enabled for spawning

	//Scoring
	private int playerScore = 0;
	private float multiplierScore = 1;
	private bool multiplierActive = false;
	private bool multiplierTimerActive = false;
	private float multiplierDuration = 4f;
	private float multiplierTimer;


    //private float spawnRange = 500f;
    
    

	/*----------------------------*/
	// Assigns Singleton instance //
	private void Awake()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		}else{
			_instance = this;
		}
	}
	 
	/*------------------------*/
	//Singleton Getter Method //
	public static GameManager Instance { get { return _instance; } }


	/*------------------------*/
	// Initialises Game Build //
    void Start()
    {
		//Checks for personality type assigned within main menu
		if (DataSaver.saver != null) {
			gameCondition = DataSaver.saver.hexType;
		}

		//sets initial position by taking  (no. tiles/2 e.g. 12/2 = 6) and multiplying players forward position by 16f * tile count
		PlayerController.Instance.transform.position += Vector3.forward * 16.0f * conquerorPrefab.transform.localScale.z* activeRoadTiles / 2.0f;

		//Enables spawning of enemies/objects
        spawn = true;

		//Enables Chaser if in survivor gameplay
		if (gameCondition == personalityType.Survivor) {
			chaser.SetActive (true);
        }
        //Or enables score multiplier for conqueror
        else
        {
            multiplierActive = true;
        }

		//Sets up our initial build of the map
        initialBuild();
    }
		

	/*------------------------------------*/
	//Creates the initial build of the map//
    public void initialBuild()
    {
		
		//populates a number of tile prefabs equal to our activeRoadTiles value
        for (int x = 0; x < activeRoadTiles ; x++)
        {
			//instantiates a new tile prefab at the position of the game manager
			if (gameCondition == personalityType.Conqueror) {
				section = (GameObject)Instantiate (conquerorPrefab, transform.position, transform.rotation);
			} else {
				section = (GameObject)Instantiate (survivorPrefab, transform.position, transform.rotation);
			}
           
			//If initial section, disable collider trigger
			if(x <= activeRoadTiles /2){section.GetComponent<levelTile>().disableTrigger();}

			//Sets the parent of the section to equal the section Holder
			section.transform.parent = sectionHolder.transform;

			//Adds the section to our internal list of sections
            sections.Add(section);
            
			//Adds the second half of road tiles created to the spawnPositions list, enabling
			//Prefabs to be created on the map
			if (x > activeRoadTiles / 2 )
            {
				activeSections.Add (section.GetComponent<levelTile> ());
            }

			//Sets the tile position foward by 16f, of which the next tile will be instantiated at
            transform.position +=transform.forward * 16.0f * conquerorPrefab.transform.localScale.z;
        }
    }

	/*------------------------------------------------------------------------------------------------*/
	//Spawns a new tile when the body trigger of the player collides (called by the player controller)//
    public void spawnTile()
    {
        //Removes the first tile in the buffer and destroys the gameobject- FIFO system.
        Destroy(sections[0]);
        sections.RemoveAt(0);

		//Removes first active tile, moving spawn positions further along
		activeSections.RemoveAt (0);

        //Instantiates our new tile prefab
		if (gameCondition == personalityType.Conqueror) {
			section = (GameObject)Instantiate (conquerorPrefab, transform.position, transform.rotation);
		} else {
			section = (GameObject)Instantiate (survivorPrefab, transform.position, transform.rotation);
		}

		//sets the tiles parent to that of the section holder
        section.transform.parent = sectionHolder.transform;

        //Adjust tile to current HIIT condition -- Starts off as high so only low transition required
        if(HIITController.Instance.currentState == ExerciseProtocolState.highIntensity)
        {
            section.GetComponent<levelTile>().lowIntensityTransition();
        }
     

		//adds the new tile to the list of sections & generates enemies if spawn is true
        sections.Add(section);
		activeSections.Add (section.GetComponent<levelTile> ());
		if (spawn) {
			spawnObjectsTile (section.GetComponent<levelTile> ());
		}

		//Moves game manager position along for next tile
        transform.position = transform.position + (transform.forward * 16.0f * conquerorPrefab.transform.localScale.z);
    }

	/*----------------------------------------------*/
	//Transitions Tiles dependent on exercise state //
	public void transitionTile(){
        ExerciseProtocolState state = HIITController.Instance.currentState;
        for (int i = 0; i < sections.Count; i++) {
			if (state == ExerciseProtocolState.lowIntensity) {
				sections [i].GetComponent<levelTile> ().lowIntensityTransition (); //Transition tiles graphically to low
				if (activeSections.Contains (sections [i].GetComponent<levelTile> ())){
					spawnObjectsTile (sections [i].GetComponent<levelTile> ());
				}
			} else if (state == ExerciseProtocolState.highIntensity) {
				sections [i].GetComponent<levelTile> ().highIntensityTransition (); //Transition tiles graphically to high
			} else {
				spawn = true;
				if (activeSections.Contains (sections [i].GetComponent<levelTile> ())){
					spawnObjectsTile (sections [i].GetComponent<levelTile> ());
				}
			}
		}
	}
		
	/*--------------------------------------------*/
	//Used by the HIITController Exercise protocol// NEW ADAPTION
	public void spawnObjectsTile(levelTile t){
		
			//Spawn certain amount of game componenets within the tile
			if (gameCondition == personalityType.Conqueror) {
				t.generateEnemies (conquerorGameComponents, maxCount, PlayerController.Instance.gameObject);
			} else {
				t.generateEnemies (survivorGameComponents, maxCount, PlayerController.Instance.gameObject);
			}
	}


	/*-------------------------------------------------------------------------*/
	//Clears all game components when transitioning from low intensity to high //
	public void clearGameComponents(){
		for(int z = enemyList.Count-1; z >= 0 ; z--){
			Destroy (enemyList [z]);
			enemyList.RemoveAt (z);
		}
	}


	/*------------------------------*/
	//Called by the HIIT Controller //
	public void setSpawning(bool condition)
    {
        spawn = condition;
    }


	/*----------------------------------------------------------*/
	// Adjusts player score when colliding with hostile element //
	public void adjustPoints(int value){

        //Subtracting points
        if (value < 0) {
			playerScore = playerScore + value > 0 ? playerScore + value : 0; //subtract value to a limit of 0
			multiplierTimer = 0; //Immediately resets multiplier 
			gui.updateScore(playerScore,true); //Tell ui to update score and display hit animation
		//Adding points
		} else if (value > 0) {
            playerScore += Mathf.FloorToInt(value*multiplierScore); //increases score

			if(multiplierActive){ //If game mode enables multiplier
				multiplierScore += 0.1f; //increments multiplier score
				multiplierTimer = multiplierDuration; //Resets multiplier duration
				if (!multiplierTimerActive) { //If a timer coroutine isn't already running
					multiplierTimerActive = true; 
					StartCoroutine (scoreMultiplier ());
				}
			gui.updateScore (playerScore); //Tells ui to update score 
			}
		}
	}

	/*---------------------------------------*/
	//Keeps track of score multiplier uptime //
	IEnumerator scoreMultiplier(){
        while (multiplierTimer > 0) { //Multiplier timer ticks down until deactivation
			multiplierTimer -= Time.deltaTime;//decrements timer
			gui.updateMultiplier(multiplierScore,multiplierTimer,true);//tells ui to update multiplier
			yield return null;
		}
		multiplierScore = 1;
		multiplierTimerActive = false;
		gui.updateMultiplier(multiplierScore,multiplierTimer,false);//tells ui to update multiplier
	}


	//Restarts The exercise sprint  -- Currently unused
   /* public void restartSprint()
    {
        //Destroy everything then initial build.
        while (vehicles[0] == null)
        {
            vehicles.RemoveAt(0);
        }
        while(vehicles.Count != 0)
        {
            removeVehicle();
        }
        while(sections.Count != 0)
        {
            Destroy(sections[0]);
            sections.RemoveAt(0);
        }

        transform.position = Vector3.zero;
        initialBuild();
    }
    */

//    public void endSession(bool beatGhost)
//    {
//        float[] playerSpeedRecording;
//        float[] playerLeanRecording;
//
//        //Convert track recording to array and save if it was LONGER.
//        if (sectionRecording.Count > prevSectionRecording.Length)
//        {
//            prevSectionRecording = sectionRecording.ToArray();
//        }
//        if(truckRecording.Count > prevTruckRecording.Length)
//        {
//            prevTruckRecording = truckRecording.ToArray();
//        }
//        Time.timeScale = 0;
//
//        //Get ghost information only if score is higher otherwise its the same
//        if (beatGhost)
//        {
//            playerSpeedRecording = player.GetComponent<PlayerController>().getPlayerSpeedRecording();
//            playerLeanRecording = player.GetComponent<PlayerController>().getPlayerLeanRecording();
//        }
//        else
//        {
//            playerSpeedRecording = DataSaver.saver.ghostSpeedRecording;
//            playerLeanRecording = DataSaver.saver.ghostLeanRecording;
//        }
//        
//        DataSaver.saver.Save(prevSectionRecording, prevTruckRecording, playerSpeedRecording, playerLeanRecording);
//        resultsButton.SetActive(true);
//    }
}