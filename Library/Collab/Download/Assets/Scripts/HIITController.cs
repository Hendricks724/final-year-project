﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HIITController : MonoBehaviour {

    public astronautController astronautController;
    public GameManager gameManager;
    public motionTypeTracker motionTypeTracker;
    public motionEnhancer motionEnhancer;
    public MeshCollider ground;


    [Header("Exercise Parameters")]
    public float intervalDuration;
    public float intervalNumber;

    [Header("Game Components")]
    public GameObject truck;
    public GameObject lava;

    //private vars
    private int currentIntervals;

	// Use this for initialization
	void Start () {
        currentIntervals = 0;
        StartCoroutine(checkTracking());
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    IEnumerator checkTracking()
    {
        while (!motionTypeTracker.Tracking)
        {
            yield return new WaitForSeconds(0.5f);
        }
        StartCoroutine(lowIntensity());
    }

    public float multiplier;
    public float probability;

    IEnumerator lowIntensity()
    {

        if (intervalNumber > currentIntervals)
        {
            yield return new WaitForSeconds(1.1f);//waiting for field to clear up
            currentIntervals++;
            //initiate setup for start of low intensity phase
            gameManager.spawnObjectsRandom(lava,Mathf.Max(8,8* (int)Mathf.Ceil((motionEnhancer.calculateBaseRunningSpeed(1 / 53f) - 1) * multiplier)), probability, true);

            yield return new WaitForSeconds(intervalDuration);

            //finish low intencity phase
            gameManager.stopSpawning();
            StartCoroutine(deactivateGround());

            StartCoroutine(highIntensity());
        }

        
    }

    IEnumerator highIntensity()
    {

        if (intervalNumber > currentIntervals)
        {
            yield return new WaitForSeconds(1.1f);//waiting for field to clear up
            currentIntervals++;
            //initiate setup for start of high intensity phase
            gameManager.spawnObjectsRandom(truck,(int)Mathf.Max(8,8* Mathf.Ceil( (motionEnhancer.calculateBaseRunningSpeed(1/53f)-1)*2)),1,false);
            astronautController.activate();

            yield return new WaitForSeconds(intervalDuration);

            //finish high intencity phase
            astronautController.deactivate();
            gameManager.stopSpawning();
            StartCoroutine(deactivateGround());

            StartCoroutine(lowIntensity());
        }

        
    }

    IEnumerator deactivateGround()
    {
        ground.enabled = false;
        yield return new WaitForSeconds(1);
        ground.enabled = true;
    }
}
